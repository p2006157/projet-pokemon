/* ******************************************************************
 * Constantes de configuration
 * ****************************************************************** */
const apiKey = "4459319c-6027-490d-9cbd-cac5245d3cdc";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */
//majEtatEtPage(etatCourant,{PokedexNumber : liste[0]["PokedexNumber"]});

/**
 * Fait une requête GET authentifiée sur /whoami qui test si la clée est 
 * la bonne test avec "apiKeygen"
 * 
 * @param {*} apiKeygen la clée de connexion pour le login
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(apiKeygen) {
  return fetch(serverUrl + "/whoami", { headers: { "Api-Key": apiKeygen } })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}




/**
 * Fait une requête GET authentifiée sur /pokemon
 * 
 * @returns une promesse de la table de pokemons ou une erreur
 */
function fetchpokemon() {
  return fetch(serverUrl + "/pokemon")
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}


/**
 * Fait une requête GET authentifiée sur /deck avec le login de l'utilisateur
 * (ici la réponce de la fonction fetchWhoami) et comme valeur"d'Api-Key" 
 * la clée donnée par l'utiisateur dans la modal de connexion
 * 
 * @param {*} login la personne de qui on veux récupérer le dec de pockemon
 * @param {*} apiKeygen la clée de connexion pour le login
 * @returns une promesse de la table de pokemons de l'utilateur identifier ou une erreur
 */
function fetchdeck(login, apiKeygen) {
  return fetch(serverUrl + "/deck/"+ login, { headers: { "Api-Key": apiKeygen } })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}


/**
 * Déclenche la mise à jour de la page en changeant l'état courant pour que la
 * modale de login soit affichée
 * 
 * @param {Etat} etatCourant  l'état courant
 */
function Aff_Modal(etatCourant) {
  majEtatEtPage(etatCourant, {
    loginModal: true,
  });
}

/**
 * Permet de lancer la fonction "fetchdeck" et de metter a jour 
 * l'état courant avec ces réponces, la valeur login est mise a jour 
 * avec le nom de l'utilisateur (vaut undefined en cas d'erreur),
 * la valeur errLogin prend les valeur d'erreur si il y en a, 
 * loginModal reste a faux si il y a eu une erreur dans la connexion
 *  sinon passe a vrais et donc desaffiche la modal et myPokemon prend 
 * la table retourner par "fetchdeck" et donc le deck de l'utilisateur
 * 
 * @param {Etat} etatCourant  l'état courant
 * @param {*} dataW  Objet contenant le nom de l'utilaseur (issue de la fonction "fetchWhoami")
 * @param {*} apiKeygen la clée de connexion pour le login
 * @returns une promesse de la table de pokemons de l'utilateur identifier ou une erreur
 */
function lancefetchdeck(etatCourant, dataW, keygen) {
  return fetchdeck(dataW.user, keygen).then((data) => {
    majEtatEtPage(etatCourant, {
      login: dataW.user, // qui vaut undefined en cas d'erreur
      errLogin: data.err, // qui vaut undefined si tout va bien
      loginModal: data.err === undefined ? false : true, // on desaffiche la modale
      myPokemons: data,
    });
  });
}


/**
 * Permet de lancer les fonction de connexion et de récupération du deck de 
 * l'utilisateur 
 *
 * @param {Etat} etatCourant l'état courant
 * @param {*} keygen la clée qui permet d'identifier la connction de l'utilateur
 * @returns Une promesse de la connexion de l'utilateur ou une errreur
 */
function lanceWhoamiEtInsereLogin(etatCourant, keygen) {
  return fetchWhoami(keygen).then((data) => {
    lancefetchdeck(etatCourant, data, keygen);
  });
}

/**
 * Peret de déconnecter l'utilateur donc repasser le login à undefined ainci
 *  que le errLogin, remet le loginModal àfalse pour pouvoir réafficher la 
 * modal de connexion
 *
 * @param {Etat} etatCourant l'état courant
 */
 function deconnexion(etatCourant) {
  majEtatEtPage(etatCourant, {
    login: undefined,
    errLogin: undefined,
    loginModal: false,
  });
}

/**
 * Génère le code HTML du corps de la modale de login avec un formulaire 
 * qui comprend un champs "password" et un bouton "submit".
 * On renvoie en plus un objet callbacks contenant les foncton a faire 
 * lors que l'on soumet le formulaire.
 * 
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et un objet 
 * lancant la fonction "lanceWhoamiEtInsereLogin" lorsque l'on soumet le 
 * formulair de connexion dans le champ callbacks
 */
function genereModaleLoginBody(etatCourant) {
  return {
    html: `
  <section class="modal-card-body">
      <form method="post" id="form_connexion">
        <input type="password" id="form_connexion_mdp"></input>
        <input type="submit"></input>
      </form>
    </section>
  `,
    callbacks: {
      "form_connexion": {
        onsubmit: () => {
          lanceWhoamiEtInsereLogin(etatCourant, 
            document.getElementById("form_connexion_mdp").value);
          return false;
        },
      }
    },
  };
}

/**
 * Génère le code HTML du titre de la modale de login et les callbacks associés
 * ici lorsque l'on click sur "btn-close-login-modal1" la modal se ferme .
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginHeader(etatCourant) {
  return {
    html: `
<header class="modal-card-head  is-back">
  <p class="modal-card-title"> Renter votre clée d'utilisateur</p>
  <button
    id="btn-close-login-modal1"
    class="delete"
    aria-label="close"
    ></button>
</header>`,
    callbacks: {
      "btn-close-login-modal1": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML du base de page de la modale de login et les callbacks associés
 * ici lorsque l'on click sur "btn-close-login-modal2" la modal se ferme  .
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginFooter(etatCourant) {
  return {
    html: `
  <footer class="modal-card-foot" style="justify-content: flex-end">
    <button id="btn-close-login-modal2" class="button">Fermer</button>
  </footer>
  `,
    callbacks: {
      "btn-close-login-modal2": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML de la modale de login et les callbacks associés
 * le champs "activeClass" permet de ne pas afficher la modal lorsque 
 * tant que l'utilateur n'as pas clicker dessus.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLogin(etatCourant) {
  const header = genereModaleLoginHeader(etatCourant);
  const footer = genereModaleLoginFooter(etatCourant);
  const body = genereModaleLoginBody(etatCourant);
  const activeClass = etatCourant.loginModal ? "is-active" : "is-inactive";
  return {
    html: `
      <div id="mdl-login" class="modal ${activeClass}">
        <div class="modal-background"></div>
        <div class="modal-card">
          ${header.html}
          ${body.html}
          ${footer.html}
        </div>
      </div>`,
    callbacks: { ...header.callbacks, ...footer.callbacks, ...body.callbacks },
  };
}

/* ************************************************************************
 * Gestion de barre de navigation contenant en particulier les bouton Pokedex,
 * Combat et Connexion.
 * ****************************************************************** */

/**
 * permet de doit lancer lamodal de coennction soit la fonction de déconnexion 
 * en fonction de "etatCourant.login" qui dit si l'utilisateur est connecter
 * 
 * @param {Etat} etatCourant
 */
function afficheModaleConnexion(etatCourant) {
  if(etatCourant.login === undefined)
    Aff_Modal(etatCourant);
  else
    deconnexion(etatCourant);
}

/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient le bouton de login.
 * En fonction de si l'utilisateur est connecter ou non le boutont seras
 * remplit de "Connexion" ou "Deconnexion". Si l'utilisateur est connecter son
 * nom est marquer à côter du boutont.
 * 
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonConnexion(etatCourant) {
  return {
    html: `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-open-login-modal" class="button is-light">
          ${etatCourant.login === undefined ? `Connexion` : `Deconnexion`}
        </a>
        ${etatCourant.login === undefined ? `` : `<p>${etatCourant.login}</p>`}
      </div>
    </div>
  </div>`,
    callbacks: {
      "btn-open-login-modal": {
        onclick: () => afficheModaleConnexion(etatCourant),
      },
    },
  };
}

/**
 * Génère le code HTML de la barre de navigation et les callbacks associés.
 * 
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBarreNavigation(etatCourant) {
  const connexion = genereBoutonConnexion(etatCourant);
  return {
    html: `
  <nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar">
      <div class="navbar-item"><div class="buttons">
          <a id="btn-pokedex" class="button is-light"> Pokedex </a>
          <a id="btn-combat" class="button is-light"> Combat </a>
      </div></div>
      ${connexion.html}
    </div>
  </nav>`,
    callbacks: {
      ...connexion.callbacks,
      "btn-pokedex": { onclick: () => 
        majEtatEtPage(etatCourant,{recherche : ""}) },
    },
  };
}

/**
 * 
 * Génère le code HTML de la barre de recherche et le callbacks associés.
 * 
 * @param {Etat} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function genererRecherchePokemon(etatCourant) {
  return {
    html: `
      <div class="tabs is-centered">
        <form>
          <input type="text" id="recherche" value="${etatCourant.recherche}" 
          placeholder="entrer pokémon !"></input>
        </form>
      </div>`,
    callbacks: {     
      "recherche" : {onchange: () => majEtatEtPage(etatCourant, 
        { recherche:document.getElementById("recherche").value, })
    },},
  };
} 

/**
 * Génère le code HTML de la barre de la barrede Navication et le callbacks
 * associés.Si l'utilastaeur est connecter affiche "Mes pokemons" sinon non.
 * 
 * @param {Etat} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici si l'utilisateur à
 * clicker sur "Tous les pokemons" la navication passe à 1 et si il a clicker
 * sur "Mes pokemons" la navication passe à 0)
 */
function Switch_Nav(etatCourant) {
  const navigation = etatCourant.navigation;
  const login = etatCourant.login;
  return {
    html: `
      <div class="tabs is-centered">
        <ul>
          <li ${navigation === 0 ? `class="is-active"` : ``} 
            id="tab-all-pokemons">
            <a>Tous les pokemons</a>
          </li>
          ${login !== undefined ? `<li 
            ${navigation === 1 ? `class="is-active"` : ``} 
          id="tab-tout"><a>Mes pokemons</a></li>` : ``}
        </ul>
      </div>`,
    callbacks: {
        "tab-tout" : { //aller a la page mes pokemons
          onclick: () => majEtatEtPage(etatCourant, { navigation: 1 })
        },
        "tab-all-pokemons" : {  //aller a la page tous les pokemons
          onclick: () => majEtatEtPage(etatCourant, { navigation: 0 })
        },
    }
  };
}

/**
 * lance la fonction "fetchpokemon" qui recupaire les pokemons ,les trie en 
 * utiliant "triePokemons" et les stock dnas la variable "pokemon" de
 * l'état courant pour ne pas avoir a refaire de get pour re-récuperer
 * les pokemon sur le serveur.
 * (pour ne pas boucler à l'infinie avec le "majEtatEtPage" la fonction
 * est appeler 1 fois a l'initialisatin de le page)
 * 
 * @param {Etat} etatCourant 
 * @returns une promesse de la table de pokemon obtenu par "fetchpokemon"
 */
function setpokemon(etatCourant) {
  return fetchpokemon().then((data) => {
    const table = triPokemons(etatCourant, data);
    majEtatEtPage(etatCourant, {
      pokemon: table,
      errPokemon: data.err,
    });
  });
}

/**
 * trie les pokemon par ordre croissant des "PokedexNumber"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre croissant des "PokedexNumber"
 */
function triPokemons(etatCourant, pokemon) {
  return pokemon.sort(function (a,b) { 
    return a.PokedexNumber-b.PokedexNumber; });
}

/**
 * trie les pokemon par ordre alphabétique des "Name"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique des "Name"
 */
function triCname(etatCourant, pokemon) {
  return pokemon.sort((a, b) => 
  a.Name.localeCompare(b.Name, 'fr', {ignorePunctuation: true}));
}

/**
 * trie les pokemon par ordre alphabétique des "Abilities"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique des "Abilities"
 */
function triCCapacite(etatCourant, pokemon) {
  return pokemon.sort((a, b) => 
  a.Abilities[0].localeCompare(
    b.Abilities[0], 'fr', {ignorePunctuation: true}));
}

/**
 * trie les pokemon par ordre alphabétique des "Types"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique des "Types"
 */
function triCType(etatCourant, pokemon) {
  return pokemon.sort((a, b) => 
   a.Types[0].localeCompare(b.Types[0], 'fr', {ignorePunctuation: true}));
}

/**
 * trie les pokemon par ordre décroissant des "PokedexNumber"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre décroissant des "PokedexNumber"
 */
function triDPokemons(etatCourant, pokemon) {
    return pokemon.sort(function (a,b) 
    { return b.PokedexNumber-a.PokedexNumber; });
}

/**
 * trie les pokemon par ordre alphabétique inverser des "Name"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique inverser des "Name"
 */
function triDname(etatCourant, pokemon) {
  return pokemon.sort((a, b) => 
  b.Name.localeCompare(a.Name, 'fr', {ignorePunctuation: true}));
}

/**
 * trie les pokemon par ordre alphabétique inverser des "Abilities"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique inverser des "Abilities"
 *     b.Abilities[0].localeCompare(a.Abilities[0], 'fr', {ignorePunctuation: true}),
 */
function triDCapacite(etatCourant, pokemon) {
  return pokemon.sort((a,b) => 
    b.Abilities[0].localeCompare(
      a.Abilities[0], 'fr', {ignorePunctuation: true}));
}

/**
 * trie les pokemon par ordre alphabétique inverser des "Types"
 * @param {Etat} etatCourant 
 * @param {Array} pokemon tableau de pokemon avec plusieur attribue
 * @returns un tableau de pokemon trier pour ordre alphabétique inverser des "Types"
 */
function triDType(etatCourant, pokemon) {
  return pokemon.sort((a,b) => 
  b.Types[0].localeCompare(
    a.Types[0], 'fr', {ignorePunctuation: true}));
}

/**
 * utilise les fonction de trie tableau en fonction du sens de trie
 * contenu dan sla vriable "trie"
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns renvoie le tableau de pokemon trié dans le bonne ordre en fonction
 * de la variable "trie" de l'état courant
 */
function tab_pok_trie(etatCourant){
  if (etatCourant.trie === 1)
    return triPokemons(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 2)
    return triDPokemons(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 3)
    return triCname(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 4)
    return triDname(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 5)
    return triCType(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 6)
    return triDType(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 7)
    return triCCapacite(etatCourant,etatCourant.pokemon);
  if (etatCourant.trie === 8)
    return triDCapacite(etatCourant,etatCourant.pokemon);
  else
    console.log("pas normal que je m'affiche");
}

/**
 * Permet de réduire la liste des pokemon a afficher en fonction de la bart de 
 * recheche et du nombre de pockemon a afficher.
 * 
 * "indexOf" renvoie -1 si l'élément n'est pas dans la table 
 * 
 * @param {Etat} etatCourant l'etat courant
 * @returns la liste des pokemon en fonction de se qu'il y a dans la bar de recheche
 */
function Limit_tab_pok_tout_pok(etatCourant) {
  const recherche = etatCourant.recherche;
  const tabpok = tab_pok_trie(etatCourant);
  if(recherche !== "")
      return tabpok
      .filter(d => d.Name.toLowerCase().indexOf(recherche.toLowerCase()) !== -1)
      .filter((d, indice) => indice < etatCourant.taille && indice >= 0);
  else
    return tabpok.filter((d, indice) => indice < etatCourant.taille && indice >= 0);
}

/**
 * Permet de réduire la liste mes pokemon a afficher en fonction de la bart de 
 * recheche et du nombre de pockemon a afficher.
 * 
 * @param {Etat} etatCourant l'etat courant
 * @returns la liste des pokemon en fonction de se qu'il y a dans la bart de recheche
 */
function Limit_tab_pok_mes_pok(etatCourant) {
  const recherche = etatCourant.recherche;
  const tabpok = tab_pok_trie(etatCourant);
  const mesPokemons = etatCourant.myPokemons.map(e => tabpok
    .filter(d => d.PokedexNumber === e)[0])
    .filter((d, indice) => indice <= etatCourant.taille && indice > 0);
  if(recherche !== "")
    return mesPokemons 
    .filter(d => d.Name.toLowerCase().indexOf(recherche.toLowerCase()) !== -1)
    .filter((d, indice) => indice < etatCourant.taille && indice >= 0);
  else
    return mesPokemons;
}

/**
 * Permet de réduire la liste des pokemon a afficher en fonction de la bart de 
 * recheche et du nombre de pockemon a afficher.
 * 
 * "indexOf" renvoie -1 si l'élément n'est pas dans la table 
 * 
 * @param {Etat} etatCourant l'etat courant
 * @returns la liste des pokemon en fonction de se qu'il y a dans la bar de recheche
 */
function Limit_tab_pok(etatCourant) {
  const recherche = etatCourant.recherche;
  const tabpok = tab_pok_trie(etatCourant);
  if(etatCourant.login === undefined || etatCourant.navigation === 0) {
    return Limit_tab_pok_tout_pok(etatCourant);
  }
  else {
    return Limit_tab_pok_mes_pok(etatCourant);
  }
}

/**
 * la fonction ajoute 10 a la taille du tableau a afficher
 * @param {Etat} etatCourant 
 */
function addPokemons(etatCourant) {
  majEtatEtPage(etatCourant, {
    taille: etatCourant.taille+10,
  });
}

/**
 * la fonction enlève 10 a la taille du tableau a afficher si celle-ci
 * est supérieur a 20
 * @param {Etat} etatCourant 
 */
function removePokemons(etatCourant) {
  majEtatEtPage(etatCourant, {
    taille: etatCourant.taille >= 20 ? 
    etatCourant.taille-10 : etatCourant.taille,
  });
}

/**
 * Permet de parcourir l'objet et d'écrire ses élément sous forme de ligne html 
 * 
 * @param {Object} objet
 * @returns
 */
function ligneObjet(objet) {
  return objet.reduce((acc, e) => acc +  [acc === `<li>` + e + `</li>` ? ``
  : `<li>` + e + `</li>`], ``); //pour pas doublons de electric pour raichu
}

/**
 * permet d'écrire les résistance d'un pokemon sous forme html
 * @param {Objet} objet 
 * @returns 
 */
function getResistance(objet) {
  const arr =Object.keys(objet);
  return ligneObjet(arr.filter(a =>objet[a] < 1));
}

/**
 * permet d'écrire les faiblesse d'un pokemon sous forme html
 * @param {Objet} objet 
 * @returns 
 */
function getWeak(objet) {
  const arr =Object.keys(objet);
  return ligneObjet(arr.filter(a =>objet[a] > 1));
}

/**
 * Génère le code HTML du titre de card de détail du pokemon et les callbacks associés
 * 
 * @param {Etat} etatCourant l'état courant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function generePokemonDetailheader (etatCourant){
  const pokemonTemp = triPokemons(etatCourant,etatCourant.pokemon);
  const po =pokemonTemp[etatCourant.detailPokemon-1];
  return {html:` 
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="card-header-title">
        ${po.JapaneseName}(${po.PokedexNumber})
        </div>
      </div>
      <div class="card-content">
        <article class="media">
          <div class="media-content">
            <h1 class="title">${po.Name}</h1>
          </div>
        </article>
      </div>`,
  callbacks :{}
  } 
}

/**
 * Génère le code HTML une partie du corps du détail pokemon.
 * On renvoie en plus un objet callbacks vide.
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function generePokemonDetailBodyMediaContent(etatCourant){
  const pokemonTemp = triPokemons(etatCourant,etatCourant.pokemon);
  const po =pokemonTemp[etatCourant.detailPokemon-1];
  return{html :
   `<div class="media-content">
      <div class="content has-text-left">
        <p>Hit points: ${po.Hp}</p>
        <h3>Abilities</h3>
        <ul>${ligneObjet(po.Abilities)}</ul>
        <h3>Resistant against</h3>
        <ul>${getResistance(po.Against)}</ul>
        <h3>Weak against</h3>
        <ul>${getWeak(po.Against)}</ul>
      </div>
    </div>`,
  callbacks : {},
  }
}

/**
 * Génère le code HTML une partie du corps du détail pokemon.
 * On renvoie en plus un objet callbacks vide.
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function generePokemonDetailBodyMediaContentRight(etatCourant){
  const pokemonTemp = triPokemons(etatCourant,etatCourant.pokemon);
  const po =pokemonTemp[etatCourant.detailPokemon-1];
  return{html :
    `<figure class="media-right">
      <figure class="image is-475x475">
        <img
          class=""
          src="${po.Images.Full}"
          alt=${po.Name}
        />
      </figure>
    </figure>`,
callbacks: {},
  }
}

/**
 * Génère le code HTML le bas du détail pokemon.
 * On renvoie en plus un objet callbacks vide.
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function generePokemonDetailFooter(etatCourant){
  const login = etatCourant.login;
  return{
    html :`
      <div class="card-footer " >
        <article class="media">
          <div class="media-content">
            ${login !== undefined ? 
              `<button class="is-success button tabindex="0"> 
              Ajouter à mon deck
              </button>`
              :""}
          </div>
        </article>
      </div>`,
    callbacks: {},
  }
}

/**
/**
 * Génère le code HTML du détail pokemon et les callbacks associés
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePokemonDetail(etatCourant) {
  const header = generePokemonDetailheader(etatCourant);
  const mediacontente = generePokemonDetailBodyMediaContent (etatCourant);
  const right = generePokemonDetailBodyMediaContentRight(etatCourant);
  const footer = generePokemonDetailFooter(etatCourant);
  return {html:`
      ${header.html}
      <div class="card-content">
        <article class="media">
          ${mediacontente.html}
          ${right.html}
        </article>
      </div>
      ${footer.html}
      `,
    callbacks: {
      ...header.callbacks,...mediacontente.callbacks,...right.callbacks,
      ...footer.callbacks,
    },
  }
}


/**
 * Mise enforme HTML pour le tableau des pokemons
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici vide car pas 
 * d'utiliter)
 */
function genereTableauPokemonsBody(etatCourant) {
  const liste = Limit_tab_pok(etatCourant);
  return {
    html: liste.reduce((acc, e) => acc +
    `<tr class="pokemons ${e.PokedexNumber === etatCourant.detailPokemon ? 
      `is-selected` : ``}" >
      <td>
        <img alt="` + e.Name + `}" src="` + e.Images.Detail + `"width="64"/>
      </td>
      <td ><div class="content">` + e.PokedexNumber + `</div></td>
      <td><div class="content">` + e.Name + `</div></td>
      <td>
        <ul>${ligneObjet(e.Abilities)}</ul>
      </td>
      <td>
        <ul>${ligneObjet(e.Types)}</ul>
      </td>
    </tr>`,''),
    callbacks: {},
  }
}

/**
 * crée le bouton plus si le nombre max de pokemon n'as pas été atein
 * 
 * @param {Etat} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici le click bouton
 * qui permet d'afficher plus de pockemon)
 */
function genereBoutonPlus(etatCourant){
  return {
    html: `
      <div class="buttons">
      ${etatCourant.taille < etatCourant.maxTable ? `
      <a id="btn-more" class="button is-light">
        More
      </a>
      `: ``}`,
    callbacks: {
      "btn-more": { //afficher plus de pokemons
        onclick: () => addPokemons(etatCourant),
      },
    }
  }
}

/**
 * crée le bouton plus si le nombre de pokemon est supérieur a 10
 * @param {Etat} etatCourant 
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks (ici le click bouton
 * qui permet d'afficher moins de pockemon)
 */
 function genereBoutonMoins(etatCourant){
  return {
    html: `
      <div class="buttons">
      ${etatCourant.taille > 10 ? `
      <a id="btn-less" class="button is-light">
        Less
      </a>
      `: ``}
    </div>`,
    callbacks: {
      "btn-less": {
        onclick: () => removePokemons(etatCourant),
      },
    }
  }
}


/**
 * Genere les bouton plus et moins sous le tableau de pokemon pour
 * en afficher plus ou moins. 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonsPlusMoins(etatCourant) {
  const plus = genereBoutonPlus(etatCourant);
  const moins = genereBoutonMoins(etatCourant);
  return {
    html: `
    ${plus.html}
    ${moins.html}
    `,
    callbacks: {
      ...plus.callbacks,...moins.callbacks
    }
  }
}

/**
 * Genere la tête du tableau numérot du tableau de pokemon et gère l'affichage
 * de la flèche à côter.Lorsque l'utilisateur click sur le "#" le trableause 
 * trie de manière croissante ou décroissante. 
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereflècheNum(etatCourant){
  return{
    html:`
      <th id="num"><span>#</span>
      ${etatCourant.trie === 1 ? 
        `<span class="icon"><i class="fas fa-angle-down"></i></span>`:`
      ${etatCourant.trie === 2 ?
        `<span class="icon"><i class="fas fa-angle-up"></i></span>`:''}`}
      </th>`,
    callbacks : {
      "num" :{
        onclick : () => trieNumero(etatCourant)
      },
    }
  }
}


/**
 * Genere la tête du tableau nom du tableau de pokemon et gère l'affichage
 * de la flèche à côter.Lorsque l'utilisateur click sur le "Name" le 
 * tableau se trie de manière alphabétique ou alphabétique inverse. 
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
 function genereflècheName(etatCourant){
  return{
    html:`
      <th id= "name"><span>Name</span>
      ${etatCourant.trie === 3 ? 
        `<span class="icon"><i class="fas fa-angle-down"></i></span>`:`
      ${etatCourant.trie === 4 ?
        `<span class="icon"><i class="fas fa-angle-up"></i></span>`:''}`}
      </th>`,
    callbacks : {
      "name":{
        onclick : () => trieName(etatCourant)
      },
    }
  }
}

/**
 * Genere la tête du tableau Abilitée du tableau de pokemon et gère l'affichage
 * de la flèche à côter.Lorsque l'utilisateur click sur le "Abilities" le 
 * tableau se trie de manière alphabétique ou alphabétique inverse. 
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
 function genereflècheAbi(etatCourant){
  return{
    html:`
      <th id= "Abilite"><span>Abilities</span>
      ${etatCourant.trie === 7 ? 
        `<span class="icon"><i class="fas fa-angle-down"></i></span>`:`
      ${etatCourant.trie === 8 ?
        `<span class="icon"><i class="fas fa-angle-up"></i></span>`:''}`}
      </th>`,
    callbacks : {
      "Abilite":{
        onclick : () => trieCapacite(etatCourant)
      },
    }
  }
}

/**
 * Genere la tête du tableau type du tableau de pokemon et gère l'affichage
 * de la flèche à côter.Lorsque l'utilisateur click sur le "Types" le 
 * tableau se trie de manière alphabétique ou alphabétique inverse. 
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
 function genereflècheTyp(etatCourant){
  return{
    html:`
    <th id= "Type"><span>Types</span>
      ${etatCourant.trie === 5 ? 
        `<span class="icon"><i class="fas fa-angle-down"></i></span>`:`
      ${etatCourant.trie === 6 ?
        `<span class="icon"><i class="fas fa-angle-up"></i></span>`:''}`}
      </th>`,
    callbacks : {
      "Type":{
        onclick : () => trieType(etatCourant)
      },
    }
  }
}

/**
 * Génere la partie HTML du haut du tableau de pokemon et les callbacks
 * associer.
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereTableauPokemonsHeader (etatCourant){
  const num =genereflècheNum(etatCourant);
  const name = genereflècheName(etatCourant);
  const Type = genereflècheTyp(etatCourant);
  const Aby = genereflècheAbi(etatCourant);
  return {
    html :`
      <thead>
        <tr id="tete_tab">
          <th><span>Image</span></th>
          ${num.html}
          ${name.html}
          ${Aby.html}
          ${Type.html}
        </tr>
      </thead>`,
    callbacks: {
      ...num.callbacks,...name.callbacks,...Type.callbacks,...Aby.callbacks
    }
  }
}

/**
 * Contient les balise ouvrante de la partie HTML du tableau de
 * pokemon
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genere_balise_début_tab (etatCourant){
  return {
    html : `

          <div class="column">
            <div id="tbl-pokemons">
              <table class="table">
              `,
    callbacks :{}
  }
}

/**
 * Contient les balise fermante de la partie HTML du tableau de
 * pokemon
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genere_balise_fin_tab (etatCourant){
  return {
    html : `
          </table>
        </div>
      </div>
              `,
    callbacks :{}
  }
}

/**
 * Appel toutes les fonctions qui génère le code HTML du tableau de pokemon
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereTabPokemon(etatCourant){
  const header= genereTableauPokemonsHeader(etatCourant);
  const text = genereTableauPokemonsBody(etatCourant);
  return {
    html : `
      ${header.html}
      <tbody>
        ${text.html}
      </tbody>
      `,
    callbacks : {...header.callbacks, ...text.callbacks}
  };
}


/**
 * Appel toutes les fonctions qui génère le code HTML du tableau de pokemon
 * et du détail pokemon
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePokemons(etatCourant) {
  const baliseD = genere_balise_début_tab (etatCourant);
  const tab =genereTabPokemon(etatCourant);
  const detail = generePokemonDetail(etatCourant);
  const baliseF = genere_balise_fin_tab(etatCourant);
  return {
    html: `
      <section class="section">
        <div class="columns">
            ${baliseD.html}
            ${tab.html}
            ${baliseF.html}
          ${detail.html}
        </div>
      </section>`,
    callbacks: {
      ...baliseD.callbacks,...tab.callbacks,...detail.callbacks,
      ...baliseF.callbacks,
    }
  };
}

/**
 * Met à jours le tableau de pokemon triée dans l'ordre croissant ou décroissant
 * du "pokedexNumber",ainsi que le numéro de pokedex du pokemon a mettre en
 * détail et change la variable "trie" pour l'affichage de la flèche 
 * 
 * @param {Etat} etatCourant l'état courant
 */
function trieNumero(etatCourant) {
  if (etatCourant.trie === 1){
    majEtatEtPage(etatCourant, {  
      pokemon : triDPokemons (etatCourant , etatCourant.pokemon),trie : 2, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }else{
    majEtatEtPage(etatCourant, {  
      pokemon : triPokemons (etatCourant , etatCourant.pokemon),trie : 1, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }
}

/**
 * Met à jours le tableau de pokemon triée dans alphabétique ou alphabétique
 * inverse du "Name",ainsi que le numéro de pokedex du pokemon a
 * mettre en détail et change la variable "trie" pour l'affichage de la flèche 
 * 
 * @param {Etat} etatCourant l'état courant
 */
function trieName(etatCourant) {
  if (etatCourant.trie === 3){
    majEtatEtPage(etatCourant, {  
      pokemon : triDname (etatCourant , etatCourant.pokemon),trie : 4, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }else{
    majEtatEtPage(etatCourant, {  
      pokemon : triCname (etatCourant , etatCourant.pokemon),trie : 3, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }
}

/**
 * Met à jours le tableau de pokemon triée dans alphabétique ou alphabétique
 * inverse du "Abilities",ainsi que le numéro de pokedex du pokemon a
 * mettre en détail et change la variable "trie" pour l'affichage de la flèche 
 * 
 * @param {Etat} etatCourant l'état courant
 */
function trieCapacite(etatCourant) {
  if (etatCourant.trie === 7){
    majEtatEtPage(etatCourant, {  
      pokemon : triDCapacite (etatCourant , etatCourant.pokemon),trie : 8, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }else{
    majEtatEtPage(etatCourant, {  
      pokemon : triCCapacite (etatCourant , etatCourant.pokemon),trie : 7, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }
}

/**
 * Met à jours le tableau de pokemon triée dans alphabétique ou alphabétique
 * inverse du "Types",ainsi que le numéro de pokedex du pokemon a
 * mettre en détail et change la variable "trie" pour l'affichage de la flèche 
 * 
 * @param {Etat} etatCourant l'état courant
 */
function trieType(etatCourant) {
  if (etatCourant.trie === 5){
    majEtatEtPage(etatCourant, {  
      pokemon : triDType (etatCourant , etatCourant.pokemon),trie : 6, 
      detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }else{
    majEtatEtPage(etatCourant, {  
      pokemon : triCType (etatCourant , etatCourant.pokemon),trie : 5, 
    detailPokemon : etatCourant.pokemon[0].PokedexNumber});
  }
}




/**
 * Géenre la partie HTML de la pare de navication, le tableau de pokemon
 * et les bouton plus et moins et les callbacks associer a chacune de
 * de ses partie.
 * 
 * @param {Etat} etatCourant l'état courant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereListePokemon(etatCourant) {
  const barredeNavigationLP = Switch_Nav(etatCourant);
  const table = generePokemons(etatCourant);
  const boutons = genereBoutonsPlusMoins(etatCourant);
  return {
    html: `
      <section class="section">
        <div class="columns">
          <div class="column">
            ${barredeNavigationLP.html}
            ${table.html}
          </div>
        </div>
        ${boutons.html}
      </section>`,
    callbacks: {
      ...barredeNavigationLP.callbacks,...table.callbacks,...boutons.callbacks,
    }
  };
}

/**
 * Génére le code HTML de la page ainsi que l'ensemble des callbacks à
 * enregistrer sur les éléments de cette page.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePage(etatCourant) {
  const barredeNavigation = genereBarreNavigation(etatCourant);
  const recherche = genererRecherchePokemon(etatCourant);
  const modaleLogin = genereModaleLogin(etatCourant);
  const listePokemon = genereListePokemon(etatCourant);
  // remarquer l'usage de la notation ... ci-dessous qui permet de "fusionner"
  // les dictionnaires de callbacks qui viennent de la barre et de la modale.
  // Attention, les callbacks définis dans modaleLogin.callbacks vont écraser
  // ceux définis sur les mêmes éléments dans barredeNavigation.callbacks. En
  // pratique ce cas ne doit pas se produire car barreDeNavigation et
  // modaleLogin portent sur des zone différentes de la page et n'ont pas
  // d'éléments en commun.
  return {
    html: barredeNavigation.html + recherche.html + modaleLogin.html + 
      listePokemon.html,
    callbacks: { ...barredeNavigation.callbacks, ...recherche.callbacks, 
      ...modaleLogin.callbacks, ...listePokemon.callbacks},
  };
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Créée un nouvel état basé sur les champs de l'ancien état, mais en prenant en
 * compte les nouvelles valeurs indiquées dans champsMisAJour, puis déclenche la
 * mise à jour de la page et des événements avec le nouvel état.
 *
 * @param {Etat} etatCourant etat avant la mise à jour
 * @param {*} champsMisAJour objet contenant les champs à mettre à jour, ainsi
 * que leur (nouvelle) valeur.
 */
function majEtatEtPage(etatCourant, champsMisAJour) {
  const nouvelEtat = { ...etatCourant, ...champsMisAJour };
  console.log(nouvelEtat.trie);
  majPage(nouvelEtat);
}

/**
 * Prend une structure décrivant les callbacks à enregistrer et effectue les
 * affectation sur les bon champs "on...". Par exemple si callbacks contient la
 * structure suivante où f1, f2 et f3 sont des callbacks:
 *
 * { "btn-pokedex": { "onclick": f1 },
 *   "input-search": { "onchange": f2,
 *                     "oninput": f3 }
 * }
 *
 * alors cette fonction rangera f1 dans le champ "onclick" de l'élément dont
 * l'id est "btn-pokedex", rangera f2 dans le champ "onchange" de l'élément dont
 * l'id est "input-search" et rangera f3 dans le champ "oninput" de ce même
 * élément. Cela aura, entre autres, pour effet de délclencher un appel à f1
 * lorsque l'on cliquera sur le bouton "btn-pokedex".
 *
 * @param {Object} callbacks dictionnaire associant les id d'éléments à un
 * dictionnaire qui associe des champs "on..." aux callbacks désirés.
 */
function enregistreCallbacks(callbacks) {
  Object.keys(callbacks).forEach((id) => {
    const elt = document.getElementById(id);
    if (elt === undefined || elt === null) {
     /* console.log(
        `Élément inconnu: ${id}, impossible d'enregistrer de callbacks sur cet id`
      );*/
    } else {
      Object.keys(callbacks[id]).forEach((onAction) => {
        elt[onAction] = callbacks[id][onAction];
        //console.log(elt[onAction]);
      });
    }
  });
}

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  const page = generePage(etatCourant);
  document.getElementById("root").innerHTML = page.html;
  enregistreCallbacks(page.callbacks);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientPokemons() {
  console.log("CALL initClientPokemons");
  const etatInitial = {
    loginModal: false,
    login: undefined,
    errLogin: undefined,
    navigation: 0,
    rechercheAct: false,
    taille: 10, //taille en multiple de 10 du tableau de pokemon dans tous les pokemons
    detailPokemon: 1,
    myPokemons: undefined,
    pokemon: undefined,
    trie: 1,
    recherche: "",
    maxTable: 801,
  };
  setpokemon(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientPokemons();
});
